variables:
  MYPY_CACHE_DIR: "${CI_PROJECT_DIR}/.cache/mypy"
  PIP_CACHE_DIR: "${CI_PROJECT_DIR}/.cache/pip"
  SAST_EXCLUDED_PATHS: "spec, test, tests, tmp, venv, .cache"
  SAST_BANDIT_EXCLUDED_PATHS: "*/venv/*"

cache:
  key:
    files:
      - requirements.txt
      - requirements.dev.txt
    prefix: $CI_COMMIT_REF_NAME
  paths:
    - .cache

stages:
  - test
  - update
  - prepare
  - release

.base:
  image: python:3.11-slim
  before_script:
    - apt update && apt install make
    - python3 -m venv venv
    - source venv/bin/activate
    - python -m pip install pip-tools
    - python -m piptools sync ${REQUIREMENTS_FILE:-requirements.dev.txt}

lint:
  stage: test
  extends: .base
  interruptible: true
  script:
    - make lint
  rules:
    - if: $CI_COMMIT_TAG
      when: never
    - if: $CI_PIPELINE_SOURCE == "schedule"
      when: never
    - when: always

update-db:
  stage: update
  extends: .base
  interruptible: true
  variables:
    REQUIREMENTS_FILE: "requirements.txt"
  script:
    - make install-spacy
    - CURRENT_YEAR=$(date +'%Y')
    - CURRENT_MONTH=$(date +'%m')
    - make fetch args="${CURRENT_YEAR} ${CURRENT_MONTH}"
    - make extract args="${CURRENT_YEAR} ${CURRENT_MONTH}"
    - make analyze args="${CURRENT_YEAR} ${CURRENT_MONTH}"
    - make update args="${CURRENT_YEAR} ${CURRENT_MONTH}"
  rules:
    - if: $CI_PIPELINE_SOURCE == "schedule"
  # when: manual # WIP
  artifacts:
    paths:
      - archives
    expire_in: 1 hour

# https://docs.gitlab.com/ee/user/project/releases/#create-a-release-by-using-a-cicd-job
tag:
  stage: prepare
  extends: .base
  rules:
    - if: $CI_COMMIT_TAG
      when: never
    - if: $CI_PIPELINE_SOURCE == "schedule"
      when: never
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
  script:
    - echo "TAG=v$(python app/manage.py calver)" >> variables.env
  artifacts:
    reports:
      dotenv: variables.env

tests:
  stage: test
  extends: .base
  interruptible: true
  script:
    - make tests args="--cov-fail-under=80"
  coverage: /(?i)total.*? (100(?:\.0+)?\%|[1-9]?\d(?:\.\d+)?\%)$/
  artifacts:
    paths:
      - htmlcov
    reports:
      junit: junit.xml
      coverage_report:
        coverage_format: cobertura
        path: coverage.xml
    expire_in: 7 day
  rules:
    - if: $CI_COMMIT_TAG
      when: never
    - if: $CI_PIPELINE_SOURCE == "schedule"
      when: never
    - when: always

release:
  stage: release
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  needs:
    - job: tag
      artifacts: true
  rules:
    - if: $CI_COMMIT_TAG
      when: never
    - if: $CI_PIPELINE_SOURCE == "schedule"
      when: never
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
  script:
    - echo "Livraison Le Mot Diplomatique ${TAG}"
  release:
    name: "Le Mot Diplomatique ${TAG}"
    description: "${CI_COMMIT_MESSAGE}"
    tag_name: "${TAG}"
    ref: "${CI_COMMIT_SHA}"

sast:
  stage: test

include:
  - template: Security/SAST.gitlab-ci.yml
  - template: Security/Dependency-Scanning.gitlab-ci.yml
  - template: Security/Secret-Detection.gitlab-ci.yml
  - template: Security/Dependency-Scanning.gitlab-ci.yml

# Le Mot Diplomatique

L'historique de la première apparition d'un mot dans 
[Le Monde Diplomatique](https://www.monde-diplomatique.fr/).

Inspiré de [New New York Times](https://twitter.com/NYT_first_said).

* En ligne : https://mot-diplomatique.fr
* Code source : https://gitlab.com/cnrdck/le-mot-diplomatique

## Plan d'action

1. installer et configurer spaCy
    * [x] installer la pipeline fr
    * [x] tokeniser
2. récupérer les articles, `fetch`
    * [x] extraire les liens d'une page d'archive
    * [x] télécharger les fichiers
    * [x] gérer l'authentification sur le site
3. extraire les données, `extract`
    * [x] extraire l'article en txt brut
    * [x] extraire les métadonnées de l'article dans un csv
4. analyser les articles, `analyze`
    * [x] extraire les jetons des articles bruts
    * [x] en faire un csv
5. charger les jetons en base, `load`
    * [x] créer les articles à partir des csv
    * [x] créer les mots à partir des jetons
    * [x] créer les lemmes à partir des jetons
6. [x] mettre en place une interface d'admin
7. versionner le projet
    * [x] gitignore des fichiers sensibles (.env, pickles)
    * [x] pousser une première version présentable
8. un front
    * [x] utiliser htmx
    * [x] utiliser tailwind
    * [x] afficher les derniers mots sur la page d'accueil
    * [x] afficher des mots au hasard
    * [x] afficher un index alpha
    * [x] afficher un index par année
    * [x] afficher des graphiques
    * afficher le détail d'un mot 
        * [x] premier article
        * [x] autres apparitions
        * [x] lemmes
        * [x] précédent, suivant
        * [x] liens vers des dictionnaires / wiki pour les mots
9. optimiser les performances
    * [x] cache des requêtes (diskcache)
    * [x] cache du rendu des pages (en gérant htmx)
    * [x] mettre en place un profiler (silk)
10. [ ] documenter
11. tester
    * [x] 50%
    * [x] 75%
    * [x] 90%
    * [ ] TDD
12. déployer
    * [x] choisir un nom de domaine : mot-diplomatique.fr
    * [x] déployer une première fois
    * [x] importer les données
    * [x] automatiser déploiements
    * [ ] automatiser import des nouveaux articles 
13. [ ] bot pour tweeter les nouveaux mots

## Développement

Le projet est développé autour de [Django](https://www.djangoproject.com/) et 
[spaCy](https://spacy.io/).

### Prérequis:

* un compte sur le monde diplo pour récupérer les articles
* python >= 3.11
* `mot-diplomatique.localhost` qui pointe `localhost` dans `/etc/hosts`
* [GNU Make](https://www.gnu.org/software/make/)

### Init du projet

```sh
# récupération du code
git clone git@gitlab.com:cnrdck/le-mot-diplomatique.git
# création du venv
python3.11 -m venv venv
source venv/bin/activate.fish
# variables d'env à renseigner (voir plus bas)
cp .env.tpl .env
# init spaCy, migration, superuser
make bootstrap
# récupération de la dernière version de la CLI tailwind
make tailwind
# récupération d'une année
./manage.py fetch 1957
# Création des articles en base et extraction du texte brut
./manage.py extract 1957
# Création des mots en bdd
./manage.py analyze 1957
# Serveur de dev sur mot-diplomatique.localhost:8000
make dev
```

### Variables d'environnement

| Variable       | Type | Description                                       |
|----------------|------|---------------------------------------------------|
| LMD_EMAIL      | str  | Email du compte sur le monde diplo                |
| LMD_PASSWORD   | str  | Mot de passe du compte sur le monde diplo         |
| LMD_CSRF       | str  | `formulaire_action_args` du frm de login du diplo |
| ADMIN_PASSWORD | str  | Mot de passe pour l'admin                         |
| DEBUG          | bool | Activer le débogage django                        |
| SECRET_KEY     | str  | Clé secrète Django                                |
| ALLOWED_HOSTS  | str  | Clé secrète Django                                |
| GITLAB_TOKEN   | str  | Jeton GitLab pour déploiement auto                |
| WEBHOOK_PATH   | str  | Chemin du crochet pour déploiement auto           |
| ADMIN_PATH     | str  | Chemin de l'interface d'administration            |

### Qualité

On utilise [black](https://black.readthedocs.io), [ruff](https://github.com/charliermarsh/ruff) et [mypy](https://mypy.readthedocs.io) pour vérifier la qualité du projet. Pour
tout lancer d'un coup :

```sh
make lint
```

### Makefile

Pour lancer le serveur de dev, construire la documentation, linter le projet...

```sh
make help
```

### Tests

C'est bien les tests. On utilise [pytest](https://docs.pytest.org)

```sh
# lancer tous les tests
make tests
# passer des arguments à pytest
make tests args="-k models --lf --pdb"
```

Et quelques plugins :

* [pytest-django](https://pytest-django.readthedocs.io) 
    simplifier l'intégration avec django
* [pytest-cov](https://pytest-cov.readthedocs.io) 
    intégration avec [coverage.py](https://coverage.readthedocs.io)
* [pytest-factoryboy](https://pytest-factoryboy.readthedocs.io)
    fixtures [factory_boy](https://factoryboy.readthedocs.io)
* [django-coverage-plugin](https://pypi.org/project/django-coverage-plugin/)
    plugin coverage.py pour la prise en compte des gabarits django

## Déploiement

L'appli est suffisamment simple pour ne pas être dockerisée.

### Initialisation

Sur une debian testing

```sh
# python 3.11 et outils
apt install python3.11 git curl certbot goaccess
# utilisateur dédié
adduser --disabled-password motdiplo
```

On crée un user dédié `motdiplo` sur lequel on basculera dès que besoin.

### Installation de l'application

```sh
su - motdiplo
# création du venv
python3.11 -m venv venv
source venv/bin/activate.fish
# install de l'appli dans app
git clone https://gitlab.com/cnrdck/le-mot-diplomatique.git app
cd app
pip install -r requirements/requirements.txt -r requirements/dev-requirements.txt
# Création d'identifiants pour l'accès aux statistiques
htpasswd -c /etc/nginx/.htpasswd motdiplo
```

### Arborescence du répertoire de l'utilisateur motdiplo

* app : code source de l'app
* venv : environnement virtuel
* stats : goaccess pour les stats

### Services 

* Copier `deploy/motdiplo.service` et `deploy/motdiplo.socket` dans `etc/systemd/system`.
* Copier (et modifier) `deploy/variables.env` dans `/etc/motdiplo`
* copier `deploy/motdiplo.conf` dans `/etc/nginx/sites-available/` et faire le lien
  symbolique vers `/etc/nginx/sites-enabled/`

```sh
systemctl start motdiplo
systemctl reload nginx
```

Puis `certbot --nginx` pour configurer le SSL.

Une fois le fichier de configuration nginx modifié par certbot, ajouter au `server` des
statistiques une authentification http.

```conf
    auth_basic "Statistiques";
    auth_basic_user_file /etc/nginx/.htpasswd;
```

### Mise à jour

Le crochet (hook) accessible à l'adresse définie via le paramètre `WEBHOOK_PATH` relance
le service `motdiplo` qui déclenche la mise à jour de l'application :

```
ExecStartPre=git pull
ExecStartPre=/home/motdiplo/venv/bin/pip-sync requirements.txt requirements.dev.txt
ExecStartPre=/home/motdiplo/venv/bin/python app/manage.py migrate
ExecStartPre=/home/motdiplo/venv/bin/python app/manage.py collectstatic --clear --no-input
ExecStartPre=/home/motdiplo/venv/bin/python app/manage.py clearcache
```

Il faut autoriser l'utilisateur `motdiplo` à lancer les commandes systemctl en modifiant
le fichier sudoers (cf. dossier `deploy`).

Le crochet est automatiquement appelé par GitLab lors d'une `Release`.

## Sécurité

Si vous découvrez une faille de sécurité merci de créer un ticket en cochant la case
permettant de le rendre confidentiel.
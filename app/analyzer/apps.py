"""Analyzer."""

from django.apps import AppConfig


class AnalyzerConfig(AppConfig):
    """Config."""

    default_auto_field = "django.db.models.BigAutoField"
    name = "analyzer"

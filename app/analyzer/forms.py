"""Formulaires."""

from django import forms
from django.db.models import QuerySet


class WordMergeForm(forms.Form):
    """Choisir quel mot garder lors d'une fusion."""

    def __init__(self, queryset: QuerySet, *args, **kwargs) -> None:
        """À l'initialisation du formulaire on lui passe le qs des mots."""
        super().__init__(*args, **kwargs)
        self.fields["original"].queryset = queryset

    _selected_action = forms.CharField(widget=forms.MultipleHiddenInput)

    original = forms.ModelChoiceField(
        label="Quel mot conserver ?",
        required=True,
        widget=forms.RadioSelect,
        queryset=None,
    )

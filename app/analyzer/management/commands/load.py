"""Import des jetons en base."""

from csv import DictReader

from core.management.commands import YearMonthCommand
from django.conf import settings
from django.utils.text import Truncator
from tqdm import tqdm

from analyzer.models import Article, Lemma, Word, word_slug

DESC_LENGTH = 25


class Command(YearMonthCommand):
    """Import des jetons en base."""

    def work_on(self, year: str, month: str) -> None:
        """Import des articles, mots et lemmes en base."""
        path = settings.ARCHIVES_DIR / year / month
        articles_file = path / "articles.csv"
        if not articles_file.is_file():
            return
        with articles_file.open("r") as f:
            reader = DictReader(f)
            articles = list(reader)

        articles_bar = tqdm(articles)
        default_description = f"{month}/{year}".ljust(DESC_LENGTH)
        articles_bar.set_description(default_description)
        for article_dict in articles_bar:
            article_id = article_dict["id"]
            if not article_id.isnumeric():
                tqdm.write(f"{article_id} pas numérique")
                return
            if Article.objects.filter(id=article_id).exists() and not self.forced:
                tqdm.write(f"{article_dict['title']} déjà fait")
                return
            article = Article.objects.create(**article_dict)
            current_description = Truncator(article.title).chars(num=DESC_LENGTH)
            articles_bar.set_description(current_description.ljust(DESC_LENGTH))
            csv_file = path / f"{article.id}.csv"

            with csv_file.open("r") as f:
                reader = DictReader(f)
                tokens = list(reader)

            # tokenisation et préparation des imports par lots
            words = []
            words_throughs = []  # m2m mot / articles
            lemmas = []

            for token in tokens:
                # Si tout le mot est en majuscules et contient un point c'est
                # probablement un sigle et on le stocke tel quel, sinon on prend la
                # forme normalisée
                word = token["norm"]
                if token["text"].isupper() and "." in token["text"]:
                    word = token["text"]

                # nettoyage de dernière minute...

                # on évite les mots qui ne commencent pas par une lettre...
                if not word[0].isalpha():
                    continue

                # on évite les mots cités avec ajout entre crochet
                if "[" in word or "]" in word:
                    continue

                lemma = token["lemma"]
                lemmas.append(Lemma(text=lemma))

                # attention :
                # on part du principe que les articles sont importés dans l'ordre
                words.append(
                    Word(
                        text=word,
                        slug=word_slug(word),
                        first_used_on=article.published_on,
                        first_used_in_id=article.id,
                        lemma_id=lemma,
                        is_bogus=False,
                    )
                )
                words_throughs.append(
                    Word.articles.through(word_id=word, article_id=article.id)
                )
            Lemma.objects.bulk_create(lemmas, ignore_conflicts=True)
            Word.objects.bulk_create(words, ignore_conflicts=True)
            Word.articles.through.objects.bulk_create(
                words_throughs, ignore_conflicts=True
            )
            articles_bar.set_description(default_description)

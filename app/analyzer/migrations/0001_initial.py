"""Migration initiale.

Générée par django 4.0.4 le 2022-05-01 16:37.
"""

import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):
    """Migration."""

    initial = True

    dependencies: list[str] = []

    operations = [
        migrations.CreateModel(
            name="Article",
            fields=[
                (
                    "id",
                    models.IntegerField(
                        primary_key=True, serialize=False, verbose_name="id"
                    ),
                ),
                ("url", models.URLField(unique=True, verbose_name="URL")),
                ("title", models.TextField(verbose_name="titre")),
                ("published_on", models.DateField(verbose_name="date de publication")),
            ],
            options={
                "verbose_name": "article",
                "verbose_name_plural": "articles",
                "ordering": ["published_on", "title"],
            },
        ),
        migrations.CreateModel(
            name="Lemma",
            fields=[
                (
                    "text",
                    models.CharField(
                        max_length=255,
                        primary_key=True,
                        serialize=False,
                        verbose_name="texte",
                    ),
                ),
            ],
            options={
                "verbose_name": "lemme",
                "verbose_name_plural": "lemmes",
                "ordering": ["text"],
            },
        ),
        migrations.CreateModel(
            name="Word",
            fields=[
                (
                    "text",
                    models.CharField(
                        max_length=255,
                        primary_key=True,
                        serialize=False,
                        verbose_name="texte",
                    ),
                ),
                ("slug", models.SlugField(allow_unicode=True, verbose_name="slug")),
                (
                    "first_used_on",
                    models.DateField(
                        db_index=True, verbose_name="date de première apparition"
                    ),
                ),
                (
                    "is_bogus",
                    models.BooleanField(
                        db_index=True, default=False, verbose_name="problématique"
                    ),
                ),
                ("articles", models.ManyToManyField(to="analyzer.article")),
                (
                    "first_used_in",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name="new_words",
                        to="analyzer.article",
                    ),
                ),
                (
                    "lemma",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name="words",
                        to="analyzer.lemma",
                    ),
                ),
            ],
            options={
                "verbose_name": "mot",
                "verbose_name_plural": "mots",
                "ordering": ["text"],
            },
        ),
    ]

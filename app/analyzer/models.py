"""Modèles."""

from datetime import datetime
from typing import TYPE_CHECKING, Optional

from django.conf import settings
from django.core.cache import cache
from django.db import models
from django.db.models import Count
from django.db.models.functions import ExtractYear, Lower, Substr
from django.utils.text import slugify

if TYPE_CHECKING:
    from pathlib import Path

    from django.db.models.query import QuerySet

RANDOM_WORDS_COUNT = 30


class Article(models.Model):
    """Un article du diplo."""

    id = models.IntegerField("id", primary_key=True)
    url = models.URLField("URL", unique=True)
    title = models.TextField("titre")
    published_on = models.DateField("date de publication")

    class Meta:
        """Meta."""

        ordering = ["published_on", "title"]
        verbose_name = "article"
        verbose_name_plural = "articles"

    def __str__(self) -> str:
        """Titre."""
        return self.title

    def get_absolute_url(self) -> str:
        """Url absolue de l'article."""
        from django.urls import reverse

        return reverse("article", args=[self.id])

    @property
    def words_count(self) -> int:
        """Décompte des mots associés à l'article."""
        return self.word_set.count()

    def get_archive_path(self) -> "Path":
        """Chemin vers le dossier des archives correspondant à l'article.

        Returns:
            Path vers archives/année/mois
        """
        return (
            settings.ARCHIVES_DIR
            / f"{self.published_on.year:04}"
            / f"{self.published_on.month:02}"
        )

    def get_txt_path(self) -> "Path":
        """Chemin vers le contenu textuel de l'article.

        Returns:
            Path vers le fichier txt
        """
        return self.get_archive_path() / f"{self.id}.txt"


def alphabetical_index_cache() -> "QuerySet":
    """Cache de l'index alpha."""
    return cache.get_or_set("alphabetical_index", Word.non_bogus.alphabetical_index)


def year_index_cache() -> "QuerySet":
    """Cache de l'index annuel."""
    return cache.get_or_set("year_index", Word.non_bogus.year_index)


def new_words_by_date_cache(date: datetime) -> "QuerySet":
    """Cache des nouveaux mots pour une date (année + mois) donnée."""
    return cache.get_or_set(
        f"new_words_by_date_{date:%d_%m}", Word.non_bogus.newest(date)
    )


class WordManager(models.Manager):
    """Manager pour les mots."""

    def get_queryset(self) -> "QuerySet":
        """Exclusion des mots problématiques."""
        return super().get_queryset().filter(is_bogus=False)

    def random(self) -> list["Word"]:
        """Retourne des mots au hasard."""
        return self.order_by("?").all()[:RANDOM_WORDS_COUNT]

    def search(self, query: str) -> "QuerySet":
        """Recherche de plein texte dans les mots.

        Fonctionne avec le [suffixe][1] `*` (par défaut) et le [jeton initial][2] `^`.

        - [1]: https://www.sqlite.org/fts5.html#fts5_prefix_queries
        - [2]: https://www.sqlite.org/fts5.html#fts5_initial_token_queries
        """
        sql = """SELECT 
            *
        FROM 
            analyzer_word
        WHERE text IN (
            SELECT text FROM word_fts WHERE text MATCH %s ORDER BY rank DESC
        ) AND is_bogus = 0"""

        return self.raw(
            sql,
            params=[query],
        )

    def starts_with(self, letter: str) -> "QuerySet":
        """Mots commençant par une lettre."""
        return self.filter(text__istartswith=letter[0])

    def from_year(self, year: int) -> "QuerySet":
        """Mots d'une année."""
        return self.filter(first_used_on__year=year)

    def newest(self, date: datetime) -> "QuerySet":
        """Nouveaux mots pour une date donnée.

        Filtré par lemme utilisé qu'une seule fois.
        """
        query = (
            self.annotate(related=Count("lemma__words"))
            .filter(
                related=1,
                first_used_on__year=date.year,
                first_used_on__month=date.month,
            )
            .order_by("-first_used_on", "text")
        )

        return query.all()

    def alphabetical_index(self) -> "QuerySet":
        """Index alphabétique."""
        return (
            self.annotate(letter=Lower(Substr("text", 1, 1)))
            .values("letter")
            .annotate(count=Count("pk"))
            .order_by("letter")
        )

    def year_index(self) -> "QuerySet":
        """Index annuel."""
        return (
            self.annotate(year=ExtractYear("first_used_on"))
            .values("year")
            .annotate(count=Count("pk"))
            .order_by("year")
        )


def delete_unused_lemmas(lemmas: list["Lemma"]) -> None:
    """Supprime les lemmes parmi `lemmas` qui ne correspondent à aucun mot."""
    [lemma.delete() for lemma in lemmas if not lemma.words.count()]


class Lemma(models.Model):
    """Un lemme, la forme canonique d'un mot.

    Les mots associés sont dans `.words`.
    """

    text = models.CharField("texte", primary_key=True, max_length=255)

    class Meta:
        """Meta."""

        ordering = ["text"]
        verbose_name = "lemme"
        verbose_name_plural = "lemmes"

    def __str__(self) -> str:
        """Mot."""
        return self.text


def word_slug(text: str) -> str:
    """Slug à partir du mot."""
    return slugify(text, allow_unicode=True)


class Word(models.Model):
    """Un mot."""

    text = models.CharField("texte", primary_key=True, max_length=255)
    slug = models.SlugField("slug", allow_unicode=True)
    articles = models.ManyToManyField(Article)
    lemma = models.ForeignKey(Lemma, on_delete=models.CASCADE, related_name="words")
    first_used_in = models.ForeignKey(
        Article, on_delete=models.CASCADE, related_name="new_words"
    )
    first_used_on = models.DateField("date de première apparition", db_index=True)
    is_bogus = models.BooleanField("problématique", default=False, db_index=True)

    objects = models.Manager()
    non_bogus = WordManager()

    class Meta:
        """Meta."""

        ordering = ["text"]
        verbose_name = "mot"
        verbose_name_plural = "mots"

    def __str__(self) -> str:
        """Mot."""
        return self.text

    def save(self, *args, **kwargs) -> None:
        """Slug et màj de la date de première utilisation si possible."""
        self.slug = word_slug(self.text)
        if article := self.first_used_in:
            self.first_used_on = article.published_on
        super().save(*args, **kwargs)

    def get_absolute_url(self) -> str:
        """Url absolue du mot."""
        from django.urls import reverse

        return reverse("word", args=[self.slug])

    @property
    def previous(self) -> Optional["Word"]:
        """Mot précédent."""
        return Word.non_bogus.filter(text__lt=self.text).order_by("-text").first()

    @property
    def following(self) -> Optional["Word"]:
        """Mot suivant."""
        return Word.non_bogus.filter(text__gt=self.text).order_by("text").first()

    @property
    def related_words(self) -> "QuerySet":
        """Mots liés au même lemme."""
        return self.lemma.words.filter(is_bogus=False).exclude(text=self.text)

    @property
    def other_articles(self) -> "QuerySet":
        """Les articles sauf le premier."""
        return self.articles.all()[1:]

    @property
    def articles_count(self) -> int:
        """Décompte des articles qui contiennent le mot."""
        return self.articles.count()

    def merge_with(self, words: list["Word"]) -> None:
        """Fusion des mots `words` avec le mot courant.

        1. prend les articles des doublons et les ajoute au mot original
        2. supprime les doublons
        3. recalcule le `first_used_in` du mot original
        4. supprime les lemmes inutilisés après suppression des doublons

        Arguments:
            words: les mots à fusionner avec le mot courant
        """
        lemmas: list[Lemma] = []
        for duplicate in words:
            self.articles.add(*list(duplicate.articles.all()))
            lemmas.append(duplicate.lemma)
            duplicate.delete()
        self.first_used_in = self.articles.first()
        self.save()
        delete_unused_lemmas(lemmas)

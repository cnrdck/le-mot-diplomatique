"""Vues."""

from typing import TYPE_CHECKING

from django.contrib.auth.decorators import permission_required
from django.contrib.syndication.views import Feed
from django.core.cache import cache
from django.core.paginator import Paginator
from django.http import Http404, HttpRequest, HttpResponse, HttpResponseBadRequest
from django.shortcuts import get_object_or_404, render
from django.views.decorators.http import require_GET, require_POST
from django.views.decorators.vary import vary_on_headers

from .helpers import find_word_in_article, suffix_search_query
from .models import (
    Article,
    Word,
    alphabetical_index_cache,
    new_words_by_date_cache,
    year_index_cache,
)

if TYPE_CHECKING:
    from django.db.models.query import QuerySet

WORDS_PER_PAGE = 100
MIN_SEARCH_QUERY_LENGTH = 3


@require_GET
@vary_on_headers("HX-Request")
def home(request: HttpRequest) -> HttpResponse:
    """Accueil.

    En version htmx : uniquement une liste de mots au hasard.
    """
    if request.htmx:
        template_name = "_words.html"
        context = {"words": Word.non_bogus.random()}
    else:
        template_name = "home.html"
        last_article = cache.get_or_set("last_article", Article.objects.last)
        context = {
            "index": alphabetical_index_cache(),
            "new_words": new_words_by_date_cache(last_article.published_on),
            "last_article": last_article,
            "count_words": cache.get_or_set("count_words", Word.non_bogus.count),
            "random_words": Word.non_bogus.random(),
        }

    return render(request, template_name, context)


@require_GET
def alphabetical_index(request: HttpRequest) -> HttpResponse:
    """Index alphabétique."""
    return render(
        request,
        "alphabetical_index.html",
        {
            "index": alphabetical_index_cache(),
            "page_title": "Index alphabétique du Mot Diplomatique",
            "page_description": "Les mots classés par ordre alphabétique.",
        },
    )


@require_GET
def year_index(request: HttpRequest) -> HttpResponse:
    """Index annuel."""
    return render(
        request,
        "year_index.html",
        {
            "index": year_index_cache(),
            "page_title": "Index annuel du Mot Diplomatique",
            "page_description": "Les mots classés par année.",
        },
    )


@require_GET
@vary_on_headers("HX-Request")
def letter(request: HttpRequest, letter: str) -> HttpResponse:
    """Index de `letter`.

    Clé de cache pour les requêtes:
        letter_a_1 (page 1 de l'index des a)
    """
    letter = letter[0]
    page = request.GET.get("page", 1)
    cache_key = f"letter_{letter}_{page}"
    results = cache.get(cache_key)
    if not results:
        words = Word.non_bogus.starts_with(letter[0])
        paginator = Paginator(words, WORDS_PER_PAGE)
        results = paginator.get_page(page)
        cache.set(cache_key, results)

    context = {
        "paginator": results,
        "page_title": f"Mots en «{letter}» du Mot Diplomatique, page {page}",
        "page_description": f"Liste des mots en «{letter}» page {page}.",
    }

    if request.htmx:
        template_name = "_list.html"
    else:
        context["index"] = alphabetical_index_cache()
        template_name = "alphabetical_index.html"

    return render(request, template_name, context)


@require_GET
@vary_on_headers("HX-Request")
def year(request: HttpRequest, year: int) -> HttpResponse:
    """Index de `year`.

    Clé de cache pour les requêtes:
        year_2021_2 (page 2 de l'index 2021)
    """
    page = request.GET.get("page", 1)
    cache_key = f"year_{year}_{page}"
    results = cache.get(cache_key)
    if not results:
        words = Word.non_bogus.from_year(year)
        paginator = Paginator(words, WORDS_PER_PAGE)
        results = paginator.get_page(page)
        cache.set(cache_key, results)

    context = {
        "paginator": results,
        "page_title": f"Mots de {year} du Mot Diplomatique, page {page}",
        "page_description": f"Liste des mots de {year} page {page}.",
    }

    if request.htmx:
        template_name = "_list.html"
    else:
        context["index"] = year_index_cache()
        template_name = "year_index.html"

    return render(request, template_name, context)


@require_GET
@vary_on_headers("HX-Request")
def word(request: HttpRequest, slug: str) -> HttpResponse:
    """Détail d'un mot."""
    word = cache.get_or_set(f"word_{slug}", Word.non_bogus.filter(slug=slug).first)
    if not word:
        raise Http404("Mot introuvable")

    template_name = "_word.html" if request.htmx else "word.html"

    context = {
        "word": word,
        "page_title": f"Le Mot Diplomatique : {word}",
        "page_description": f"Première apparition du mot {word} dans le diplo.",
        "excerpt": find_word_in_article(word),
    }

    return render(request, template_name, context)


@permission_required("analyzer.edit_word'", raise_exception=True)
@require_POST
def bogus(request: HttpRequest, slug: str) -> HttpResponse:
    """Bascule d'un mot en bogus ou non et nettoyage du cache."""
    if not request.htmx:
        return HttpResponseBadRequest("Pas du htmx")
    word = get_object_or_404(Word, slug=slug)
    word.is_bogus = not word.is_bogus
    word.save(update_fields=["is_bogus"])
    cache.clear()

    return render(request, "_bogus.html", {"word": word})


@require_GET
@vary_on_headers("HX-Request")
def search(request: HttpRequest) -> HttpResponse:
    """Recherche par mots."""
    query = request.GET.get("query")
    context = {
        "page_title": "Recherche dans Le Mot Diplomatique",
        "page_description": "Moteur de recherche du Mot Diplomatique.",
        "query": query,
    }

    if query and len(query) >= MIN_SEARCH_QUERY_LENGTH:
        context["extra_params"] = f"query={query}&"
        words = Word.non_bogus.search(suffix_search_query(query))
        paginator = Paginator(words, WORDS_PER_PAGE)
        context["paginator"] = paginator.get_page(request.GET.get("page"))

    template_name = "_results.html" if request.htmx else "search.html"

    return render(request, template_name, context)


class NewWordsFeed(Feed):
    """Flux RSS."""

    title = "Nouveaux mots diplomatiques"
    link = "/"
    description = "Nouveaux mots publiés dans Le Monde Diplomatique."

    def items(self) -> "QuerySet":
        """Derniers mots."""
        last_article = cache.get_or_set("last_article", Article.objects.last)
        return new_words_by_date_cache(last_article.published_on)

    def item_title(self, item: Word) -> str:
        """Titre."""
        return str(item)

    def item_description(self, item: Word) -> str:
        """Description."""
        return (
            f"Première apparition le {item.first_used_on:%d/%m/%Y} "
            f"dans « {item.first_used_in} »"
        )


@require_GET
@vary_on_headers("HX-Request")
def article(request: HttpRequest, id: int) -> HttpResponse:
    """Détail d'un article."""
    key = f"article_{id}"
    article = cache.get_or_set(key, Article.objects.filter(pk=id).first)
    if not article:
        cache.delete(key)
        raise Http404("Article introuvable")

    template_name = "_article.html" if request.htmx else "article.html"

    context = {
        "article": article,
        "page_title": f"Le Mot Diplomatique : {article}",
        "page_description": f"Mots apparus pour la première fois dans {article}.",
    }

    return render(request, template_name, context)

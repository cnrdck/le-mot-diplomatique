"""Core."""

from django.apps import AppConfig


class CoreConfig(AppConfig):
    """Config."""

    default_auto_field = "django.db.models.BigAutoField"
    name = "core"

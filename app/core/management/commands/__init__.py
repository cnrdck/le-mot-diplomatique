"""Boite à outils pour les commandes."""

import timeit
from datetime import date, timedelta
from typing import TYPE_CHECKING

from django.core.management.base import BaseCommand

if TYPE_CHECKING:
    from django.core.management.base import CommandParser


def get_months(value: str) -> list[str]:
    """Interprète le(s) mois demandé(s) au lancement de la commande."""
    if value == "*":
        return [f"{m:02}" for m in range(1, 13)]
    return [value]


def get_years(value: str) -> list[str]:
    """Interprète l'année(s) demandée(s) au lancement de la commande."""
    if value == "*":
        return [str(y) for y in range(1954, date.today().year + 1)]
    return [value]


class YearMonthCommand(BaseCommand):
    """Commande de base pour les manipulation des archives par année/mois.

    A surclasser avec sa propre méthode `work_on` et la propriété `help`.
    """

    global_timer = None  # un timer initialisé avant la boucle des années
    forced = False  # vrai si commande lancée en --force

    def add_arguments(self, parser: "CommandParser") -> None:
        """Arguments."""
        parser.add_argument(
            "year",
            nargs="?",
            type=str,
            help="année, * pour toutes",
            default="*",
        )
        parser.add_argument(
            "month",
            nargs="?",
            type=str,
            help="mois avec zero initial, * pour tous",
            default="*",
        )

        parser.add_argument(
            "--force",
            action="store_true",
            help="Force le traitement",
        )

    def handle(self, *args, **options) -> None:
        """Lance un work_on pour chq mois & années."""
        years = get_years(options["year"])
        months = get_months(options["month"])
        self.forced = options["force"]

        self.global_timer = timeit.default_timer()
        self.pre_loop()
        for year in years:
            for month in months:
                self.work_on(year, month)

        took = timeit.default_timer() - self.global_timer

        self.stdout.write(self.style.SUCCESS(f"Terminé en {timedelta(seconds=took)}!"))

    def pre_loop(self) -> None:
        """Exécuté avant la boucle sur les mois/années."""

    def work_on(self, year: str, month: str) -> None:
        """La docstring de work_on sera affichée en aide de la commande."""
        raise NotImplementedError()

    @property
    def help(self) -> str | None:
        """Docstring de work_on."""
        return self.work_on.__doc__

"""Initialisation de l'instance."""

from django.conf import settings
from django.contrib.auth import get_user_model
from django.core.management import call_command
from django.core.management.base import BaseCommand


class Command(BaseCommand):
    """Bootstrap."""

    help = "Initialise la bdd et crée un admin."

    def handle(self, *args, **options) -> None:
        """Migrate et createsuperuser."""
        call_command("migrate")
        call_command(
            "createsuperuser",
            username=settings.ADMIN_USERNAME,
            email=settings.ADMIN_EMAIL,
            interactive=False,
        )
        User = get_user_model()
        admin = User.objects.get(username=settings.ADMIN_USERNAME)
        admin.set_password(settings.ADMIN_PASSWORD)
        admin.save()

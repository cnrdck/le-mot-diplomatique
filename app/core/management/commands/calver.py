"""Version."""

from django.conf import settings
from django.core.management.base import BaseCommand


class Command(BaseCommand):
    """Version."""

    help = "Affiche le n° de version."

    def handle(self, *args, **options) -> None:
        """Affiche le n° de version."""
        self.stdout.write(settings.VERSION)

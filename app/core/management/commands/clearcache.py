"""Vidage du cache."""

from django.core.cache import cache
from django.core.management.base import BaseCommand


class Command(BaseCommand):
    """Vidage du cache."""

    help = "Vide le cache."

    def handle(self, *args, **options) -> None:
        """Vide le cache."""
        cache.clear()
        self.stdout.write(self.style.SUCCESS("OK !"))

"""Runserver + CLI tailwind."""

import os
import subprocess
import threading

from django.conf import settings
from django.contrib.staticfiles.management.commands.runserver import (
    Command as StaticFilesRunserverCommand,
)
from django.utils.autoreload import DJANGO_AUTORELOAD_ENV


class Command(StaticFilesRunserverCommand):
    """Runserver + CLI tailwind."""

    help = "Runserver + CLI tailwind."

    def run(self, **options) -> None:
        """Lance la CLI tailwind dans un autre fil avant de lancer le serveur de dev."""
        if not os.environ.get(DJANGO_AUTORELOAD_ENV):
            self.stdout.write("Démarrage de la CLI Tailwind.")
            source = settings.TAILWIND_INPUT_CSS
            destination = settings.TAILWIND_OUTPUT_CSS
            command = f"./tailwindcss -i {source}  -o {destination} --watch --minify"
            tailwind_thread = threading.Thread(
                target=subprocess.run, args=(command,), kwargs={"shell": True}
            )
            tailwind_thread.start()
        super().run(**options)

"""Import des articles depuis le diplo."""

from django.core.management import call_command

from core.management.commands import YearMonthCommand


class Command(YearMonthCommand):
    """Import."""

    help = "Lance fetch, extract, analyze et load."

    def handle(self, *args, **options) -> None:
        """Lance fetch, extract, analyze et load."""
        params = {"year": options["year"], "month": options["month"]}
        steps = ["fetch", "extract", "analyze", "load"]
        count = len(steps)
        for index, step in enumerate(steps, 1):
            self.stdout.write(f"\n{index}/{count}: {step.upper()}\n")
            call_command(step, **params)

"""Envoi des mises à jour depuis la CI.

Besoin d'avoir dans les variables d'env :
- CI_JOB_ID : cf. variables prédéfinies GitLab CI
- MOTDIPLO_HOST : url de l'instance ciblée (ex. https://www.mot-diplomatique.fr)
- GITLAB_TOKEN : jeton pour authentifier les appels émis par la CI
"""

import os
from typing import TYPE_CHECKING

import requests
from django.core.management.base import BaseCommand
from django.urls import reverse

if TYPE_CHECKING:
    from django.core.management.base import CommandParser


class Command(BaseCommand):
    """Mise à jour."""

    def add_arguments(self, parser: "CommandParser") -> None:
        """Arguments."""
        parser.add_argument(
            "year",
            nargs="?",
            help="année",
        )
        parser.add_argument(
            "month",
            nargs="?",
            help="mois avec zero initial",
        )

    def handle(self, *args, **options) -> None:
        """Appelle le crochet de mise à jour des données."""
        year = options["year"]
        month = options["month"]
        token = os.environ["GITLAB_TOKEN"]
        job_id = os.environ["CI_JOB_ID"]
        host = os.environ["MOTDIPLO_HOST"]
        self.stdout.write(
            f"Notification de mise à disposition des données {month}/{year} à {host}"
        )
        payload = {"year": year, "month": month, "job_id": job_id}
        headers = {"X-Gitlab-Token": token}
        url = host + reverse("update")
        response = requests.post(url, data=payload, headers=headers, timeout=20)
        response.raise_for_status()
        self.stdout.write(self.style.SUCCESS("OK !"))

    help = handle.__doc__

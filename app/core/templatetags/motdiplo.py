"""Balises et filtres de gabarit."""

from django import template
from django.conf import settings

register = template.Library()


@register.simple_tag
def version() -> str:
    """Numéro de version depuis les settings."""
    return settings.VERSION

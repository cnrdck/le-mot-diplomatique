import os
from datetime import date
from io import StringIO
from unittest.mock import Mock, patch

import pytest
from django.contrib.auth import get_user_model
from django.core.management import call_command

from core.management.commands import get_months, get_years


def test_get_months_wildcard():
    months = get_months("*")
    assert min(months) == "01"
    assert max(months) == "12"
    assert len(months) == 12


def test_get_months_value():
    assert get_months("12") == ["12"]


def test_get_years_wildcard():
    years = get_years("*")
    assert min(years) == "1954"
    assert max(years) == str(date.today().year)
    assert len(years) == date.today().year - 1954 + 1


def test_get_years_value():
    assert get_years("1980") == ["1980"]


@pytest.mark.django_db
class TestBootstrap:
    def test_superuser(self, settings):
        User = get_user_model()
        with pytest.raises(User.DoesNotExist):
            User.objects.get(username=settings.ADMIN_USERNAME)
        call_command("bootstrap")
        admin = User.objects.get(username=settings.ADMIN_USERNAME)
        assert admin.is_superuser
        assert admin.check_password(settings.ADMIN_PASSWORD)


def test_calver(settings):
    settings.VERSION = "xx.y.zz"
    out = StringIO()
    call_command("calver", stdout=out)
    assert "xx.y.zz" in out.getvalue()


@pytest.mark.urls("core.tests.urls")
@patch.dict(
    os.environ,
    {
        "GITLAB_TOKEN": "token",
        "CI_JOB_ID": "12345",
        "MOTDIPLO_HOST": "https://mot-diplomatique.local",
    },
)
def test_update():
    out = StringIO()
    with patch("core.management.commands.update.requests") as mock_requests:
        post = Mock()
        mock_requests.post = Mock(return_value=post)
        call_command("update", year="1999", month="09", stdout=out)
    mock_requests.post.assert_called_once_with(
        "https://mot-diplomatique.local/hook/gitlab/update/",
        data={"year": "1999", "month": "09", "job_id": "12345"},
        headers={"X-Gitlab-Token": "token"},
        timeout=20,
    )
    assert "OK !" in out.getvalue()

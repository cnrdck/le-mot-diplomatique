from unittest.mock import Mock, patch
from zipfile import ZipFile

import pytest
from django.core.exceptions import PermissionDenied, ValidationError
from freezegun import freeze_time

from core.helpers import (
    delete_update,
    download_update,
    load_update,
    restart_service,
    validate_gitlab_token,
    validate_job_id,
    validate_update,
    validate_year_month,
)


def test_restart_service():
    with patch("core.helpers.subprocess") as mock_subprocess:
        restart_service()
        mock_subprocess.call.assert_called_once()


def test_load_update():
    with patch("core.helpers.call_command") as mock_command:
        load_update("2023", "01")
        mock_command.assert_called_once_with("load", year="2023", month="01")


def test_delete_update(settings):
    settings.GITLAB_API_URL = "http://caramail.fr"
    settings.GITLAB_TOKEN = "token"
    with patch("core.helpers.requests") as mock_requests:
        delete_update(0)
        mock_requests.delete.assert_called_once_with(
            "http://caramail.fr/jobs/0/artifacts",
            headers={"PRIVATE-TOKEN": "token"},
            timeout=60,
        )


@pytest.mark.urls("core.tests.urls")
class TestCheckGitlabToken:
    def test_ok(self, rf, settings):
        settings.GITLAB_TOKEN = "bonjour"
        headers = {"HTTP_X_GITLAB_TOKEN": "bonjour"}
        request = rf.post("/hook/gitlab/deploy/", None, **headers)
        try:
            validate_gitlab_token(request)
        except Exception as exception:  # pragma: no cover
            pytest.fail(f"ne devrait lever une {exception}")

    def test_ko(self, rf, settings):
        settings.GITLAB_TOKEN = "bonjour"
        headers = {"HTTP_X_GITLAB_TOKEN": "au revoir"}
        request = rf.post("/hook/gitlab/deploy/", None, **headers)
        with pytest.raises(PermissionDenied):
            validate_gitlab_token(request)


@freeze_time("2023-01-01")
@pytest.mark.urls("core.tests.urls")
class TestValidateYearMonth:
    @pytest.mark.parametrize(
        "data",
        [{"year": "2023", "mois": "01"}, {"année": "2023", "month": "01"}],
        ids=[
            "manque month",
            "manque year",
        ],
    )
    def test_payload(self, rf, data):
        request = rf.post("/hook/gitlab/update/", data)
        with pytest.raises(ValidationError) as exception:
            validate_year_month(request)
        assert exception.value.args[1] == "missing"

    @pytest.mark.parametrize(
        "data",
        [
            {"year": "2023", "month": "02"},
            {"year": "2024", "month": "01"},
            {"year": "1954", "month": "08"},
            {"year": "2022", "month": "13"},
        ],
        ids=[
            "Y/m+1",
            "Y+1/m",
            "avant 09/1954",
            "mois 13",
        ],
    )
    def test_invalid(self, rf, data):
        request = rf.post("/hook/gitlab/update/", data)
        with pytest.raises(ValidationError) as exception:
            validate_year_month(request)
        assert exception.value.args[1] == "invalid"

    @pytest.mark.parametrize(
        "data",
        [
            {"year": "2023", "month": "01"},
            {"year": "1954", "month": "09"},
            {"year": "2020", "month": "01"},
            {"year": "1970", "month": "12"},
        ],
        ids=[
            "max",
            "min",
            "01/YYYY",
            "12/YYYY",
        ],
    )
    def test_ok(self, rf, data):
        request = rf.post("/hook/gitlab/update/", data)
        assert validate_year_month(request) == (data["year"], data["month"])


@pytest.mark.urls("core.tests.urls")
class TestValidateJobId:
    def test_ok(self, rf):
        data = {"job_id": "12345"}
        request = rf.post("/hook/gitlab/update/", data)
        try:
            validate_job_id(request)
        except Exception as exception:  # pragma: no cover
            pytest.fail(f"ne devrait lever une {exception}")

    @pytest.mark.parametrize(
        ("data", "code"),
        [
            ({"job_id": ""}, "invalid"),
            ({"job_id": "abc"}, "invalid"),
            ({"id_job": "12345"}, "missing"),
        ],
    )
    def test_ko(self, rf, data, code):
        request = rf.post("/hook/gitlab/update/", data)
        with pytest.raises(ValidationError) as exception:
            validate_job_id(request)
        assert exception.value.args[1] == code


@pytest.fixture
def good_file_list():
    return [
        "archives/",
        "archives/2023/",
        "archives/2023/01/",
        "archives/2023/01/test.txt",
        "archives/2023/01/test.html",
        "archives/2023/01/test.csv",
    ]


@pytest.fixture
def bad_file_list():
    return [
        "app/",  # hors archives/
        "archives/2022/01",  # mauvaise année
        "archives/2023/12",  # mauvais mois
        "archives/2023/01/test.xml",  # mauvaise extension
        "archives/2023/12/test.txt",  # mauvais chemin
    ]


class TestValidateUpdate:
    def test_ok(self, good_file_list):
        try:
            validate_update(good_file_list, "2023", "01")
        except Exception as exception:  # pragma: no cover
            pytest.fail(f"ne devrait lever une {exception}")

    def test_ko(self, good_file_list, bad_file_list):
        with pytest.raises(ValidationError) as exception:
            validate_update(good_file_list + bad_file_list, "2023", "01")
        for filename in bad_file_list:
            assert filename in exception.value.error_dict


@pytest.fixture
def artefact_zip(tmp_path, good_file_list):
    file_dict = {}
    for f in good_file_list:
        p = tmp_path / f
        if f.endswith(("html", "txt", "csv")):
            p.touch()
        else:
            p.mkdir()
        file_dict[f] = p

    artefact_zip = tmp_path / "artifacts.zip"
    with ZipFile(artefact_zip, "w") as z:
        for filename, path in file_dict.items():
            z.write(path, filename)

    return artefact_zip.open(mode="rb")


class TestDownloadUpdate:
    def test_calls(self, settings):
        settings.GITLAB_API_URL = "http://caramail.fr"
        settings.GITLAB_TOKEN = "token"
        with (
            patch("core.helpers.requests") as mock_requests,
            patch("core.helpers.ZipFile") as mock_zipfile,
            patch("core.helpers.validate_update") as mock_validate,
        ):
            mock_unzipped = Mock()
            # mock du gestionnaire de contexte de ZipFile avec __enter__
            mock_zipfile.return_value.__enter__.return_value = mock_unzipped
            download_update("2023", "01")
            mock_requests.get.assert_called_once_with(
                "http://caramail.fr/jobs/artifacts/main/download?job=update-db",
                headers={"PRIVATE-TOKEN": "token"},
                timeout=60,
            )
            mock_zipfile.assert_called_once()
            mock_validate.assert_called_once()
            mock_unzipped.extractall.assert_called_once()

    @freeze_time("2023-01-01")
    def test_extract(self, settings, tmp_path, artefact_zip, good_file_list):
        settings.BASE_DIR = tmp_path
        with patch("core.helpers.requests") as mock_requests:
            mock_response = Mock()
            mock_response.raw = artefact_zip
            mock_requests.get.return_value = mock_response
            download_update("2023", "01")
        for file_name in good_file_list:
            p = tmp_path / file_name
            assert p.exists()


# def download_update(year: str, month: str) -> None:
#     """Téléchargement des artefacts de mise à jour.

#     Télécharge les artefacts du job update-db sur la branche principale, valide son
#     contenu et décompresse l'archive.

#     Args:
#         year: l'année ciblée
#         month: le mois ciblé
#     """
#     url = f"{settings.GITLAB_API_URL}/jobs/artifacts/main/download?job=update-db"
#     response = requests.get(
#         url, headers={"PRIVATE-TOKEN": settings.GITLAB_TOKEN}, timeout=60
#     )
#     with ZipFile(response.raw) as update:
#         validate_update(update.namelist(), year, month)
#         update.extractall(settings.BASE_DIR, "archives")

from http import HTTPStatus
from unittest.mock import patch

import pytest
from freezegun import freeze_time
from pytest_django.asserts import assertContains, assertNotContains, assertTemplateUsed

pytestmark = pytest.mark.django_db


class TestRobotsTxt:
    url = "/robots.txt"
    needle = "User-Agent: *"

    def test_get(self, client):
        response = client.get(self.url)
        assert response.status_code == HTTPStatus.OK
        assert response["content-type"] == "text/plain"
        lines = response.content.decode().splitlines()
        assert self.needle in lines

    def test_post_disallowed(self, client):
        response = client.post(self.url)
        assert response.status_code == HTTPStatus.METHOD_NOT_ALLOWED


class TestHumansTxt(TestRobotsTxt):
    url = "/humans.txt"
    needle = "Auteur : Renaud Canarduck"


class TestSecurityTxt(TestRobotsTxt):
    url = "/.well-known/security.txt"
    needle = "Preferred-Languages: fr, en"

    def test_expires_in_180_days(self, client, settings):
        settings.VERSION = "22.1.1.0000"
        response = client.get(self.url)
        lines = response.content.decode().splitlines()
        assert "Expires: 2022-06-30T00:00:00+02:00" in lines

    @freeze_time("2023-01-01")
    def test_invalid_version_date(self, client, settings):
        settings.VERSION = "invalid"
        response = client.get(self.url)
        lines = response.content.decode().splitlines()
        assert "Expires: 2023-06-30T00:00:00+00:00" in lines

    def test_canonical(self, client, settings):
        settings.CANONICAL_URL = "https://test.localhost"
        response = client.get(self.url)
        lines = response.content.decode().splitlines()
        assert "Canonical: https://test.localhost/.well-known/security.txt" in lines


class TestSwJsTxt:
    def test_get(self, client):
        response = client.get("/sw.js")
        assert response.status_code == HTTPStatus.OK
        assert response["content-type"] == "application/javascript"

    def test_post_disallowed(self, client):
        response = client.post("/sw.js")
        assert response.status_code == HTTPStatus.METHOD_NOT_ALLOWED


def test_404(client):
    with assertTemplateUsed("404.html"):
        response = client.get("/pas-une-vraie-page/")
    assertContains(response, "Introuvable", status_code=HTTPStatus.NOT_FOUND)
    assertContains(response, "/sw.js", status_code=HTTPStatus.NOT_FOUND)


def test_offline(client):
    with assertTemplateUsed("offline.html"):
        response = client.get("/offline.html")
    assertContains(response, "Déconnecté", status_code=HTTPStatus.OK)
    assertNotContains(response, "/sw.js", status_code=HTTPStatus.OK)


@pytest.mark.urls("core.tests.urls")
def test_500(client):
    client.raise_request_exception = False
    # with assertTemplateUsed("500.html"):
    response = client.get("/server_error/")
    assertContains(response, "Plantage", status_code=HTTPStatus.INTERNAL_SERVER_ERROR)
    assertNotContains(response, "/sw.js", status_code=HTTPStatus.INTERNAL_SERVER_ERROR)


@pytest.fixture(autouse=True)
def _webhook_settings(settings):
    """Force les paramètres du webhook gitlab."""
    settings.GITLAB_TOKEN = "ABC123"
    settings.WEBHOOK_DEPLOY_PATH = "taupe"


@pytest.mark.urls("core.tests.urls")
class TestDeployWebhook:
    def test_get(self, client):
        with patch("core.views.restart_service") as mock_restart:
            response = client.get("/hook/gitlab/deploy/")
            mock_restart.assert_not_called()
        assert response.status_code == HTTPStatus.METHOD_NOT_ALLOWED

    def test_missing_token(self, client):
        with patch("core.views.restart_service") as mock_restart:
            response = client.post("/hook/gitlab/deploy/")
            mock_restart.assert_not_called()
        assert response.status_code == HTTPStatus.FORBIDDEN

    def test_bad_token(self, client):
        with patch("core.views.restart_service") as mock_restart:
            response = client.post(
                "/hook/gitlab/deploy/",
                HTTP_X_GITLAB_TOKEN="taupe",
            )
            mock_restart.assert_not_called()

        assert response.status_code == HTTPStatus.FORBIDDEN

    def test_success(self, client):
        with patch("core.views.restart_service") as mock_restart:
            response = client.post(
                "/hook/gitlab/deploy/",
                HTTP_X_GITLAB_TOKEN="ABC123",
                content_type="application/json",
                data={"push": "push"},
            )
            mock_restart.assert_called_once()

        assert response.status_code == HTTPStatus.OK
        assert response.content.decode() == "Roger ça."


@pytest.mark.urls("core.tests.urls")
class TestUpdateWebhook:
    def test_get(self, client):
        with patch("core.views.validate_gitlab_token") as mock_check:
            response = client.get("/hook/gitlab/update/")
            mock_check.assert_not_called()
        assert response.status_code == HTTPStatus.METHOD_NOT_ALLOWED

    def test_missing_token(self, client):
        with patch("core.views.validate_year_month") as mock_validate:
            response = client.post("/hook/gitlab/update/")
            mock_validate.assert_not_called()
        assert response.status_code == HTTPStatus.FORBIDDEN

    def test_calls(self, client):
        with (
            patch(
                "core.views.validate_year_month", return_value=("2023", "01")
            ) as mock_validate,
            patch("core.views.validate_gitlab_token") as mock_check,
            patch("core.views.validate_job_id", return_value=12345) as mock_job_id,
            patch("core.views.download_update") as mock_download,
            patch("core.views.load_update") as mock_load,
            patch("core.views.delete_update") as mock_delete,
        ):
            response = client.post(
                "/hook/gitlab/update/",
            )
            mock_check.assert_called_once()
            mock_validate.assert_called_once()
            mock_job_id.assert_called_once()
            mock_download.assert_called_once_with("2023", "01")
            mock_load.assert_called_once_with("2023", "01")
            mock_delete.assert_called_once_with(12345)

        assert response.status_code == HTTPStatus.OK

    def test_invalid_date(self, client):
        with (
            patch("core.views.validate_gitlab_token"),
            patch("core.views.validate_job_id") as mock_validate_job_id,
        ):
            response = client.post(
                "/hook/gitlab/update/",
                content_type="application/json",
                data={"month": "13", "year": "2022"},
            )
            mock_validate_job_id.assert_not_called()
            assert response.status_code == HTTPStatus.BAD_REQUEST

    def test_invalid_job_id(self, client):
        with (
            patch("core.views.validate_gitlab_token"),
            patch("core.views.validate_year_month", return_value=("2000", "01")),
            patch("core.views.download_update") as mock_download,
        ):
            response = client.post(
                "/hook/gitlab/update/",
                content_type="application/json",
                data={"month": "0", "year": "0", "job_id": "0"},
            )
            mock_download.assert_not_called()
            assert response.status_code == HTTPStatus.BAD_REQUEST

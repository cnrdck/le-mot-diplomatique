"""Routeur."""

from typing import TYPE_CHECKING

from django.http import HttpResponse, HttpResponseServerError
from django.urls import path

from core.views import deploy_webhook, update_webhook

if TYPE_CHECKING:
    from django.http import HttpRequest


def server_error(request: "HttpRequest") -> HttpResponse:
    """Vue de test pour une 500."""
    return HttpResponseServerError()


def fake_view(request: "HttpRequest") -> HttpResponse:
    """Vue de test."""
    return HttpResponse()


urlpatterns = [
    path("", fake_view, name="home"),
    path("recherche/", fake_view, name="search"),
    path("lettres/", fake_view, name="alphabetical-index"),
    path("années/", fake_view, name="year-index"),
    path("hook/gitlab/deploy/", deploy_webhook, name="deploy"),
    path("hook/gitlab/update/", update_webhook, name="update"),
    path("/server_error/", server_error),
]

"""Vues."""

from datetime import datetime, timedelta

from django.conf import settings
from django.core.exceptions import BadRequest, ValidationError
from django.http import HttpRequest, HttpResponse
from django.shortcuts import render
from django.template.response import SimpleTemplateResponse
from django.urls import reverse
from django.utils import timezone
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_GET, require_POST

from .helpers import (
    delete_update,
    download_update,
    load_update,
    restart_service,
    validate_gitlab_token,
    validate_job_id,
    validate_year_month,
)


@require_GET
def robots_txt(request: HttpRequest) -> HttpResponse:
    """Fichier robots.txt."""
    lines = ["User-Agent: *", "Disallow: /admin/"]
    return HttpResponse("\n".join(lines), content_type="text/plain")


@require_GET
def humans_txt(request: HttpRequest) -> HttpResponse:
    """Fichier humans.txt."""
    lines = [
        "/* QUI ? */",
        "Auteur : Renaud Canarduck",
        "Contact : renaud à mot-diplomatique.fr",
        "Twitter : @MotDiplo",
        "",
        "/* QUOI ? */",
        "Code source : https://gitlab.com/cnrdck/le-mot-diplomatique",
        "Documentation : https://cnrdck.gitlab.io/le-mot-diplomatique/",
        "Briques utilisées : Django, htmx, spaCy, BeautifulSoup, diskcache",
    ]
    return HttpResponse("\n".join(lines), content_type="text/plain")


@require_GET
def security_txt(request: HttpRequest) -> HttpResponse:
    """Fichier security.txt.

    la date d'expiration des infos est fixée à 180j après la release courante.
    """
    try:
        current_version_datetime = timezone.make_aware(
            datetime.strptime(settings.VERSION, "%y.%m.%d.%H%M")
        )
    except Exception:
        current_version_datetime = timezone.now()
    expires = current_version_datetime + timedelta(days=180)
    lines = [
        "Contact: https://gitlab.com/cnrdck/le-mot-diplomatique#sécurité",
        f"Expires: {expires.replace(microsecond=0).isoformat()}",
        "Preferred-Languages: fr, en",
        f"Canonical: {settings.CANONICAL_URL}{reverse('security')}",
    ]
    return HttpResponse("\n".join(lines), content_type="text/plain")


@require_GET
def sw_js(request: HttpRequest) -> HttpResponse:
    """Fichier sw.js."""
    return SimpleTemplateResponse("sw.js", content_type="application/javascript")


@require_GET
def offline(request: HttpRequest) -> HttpResponse:
    """Page hors ligne pour le service worker."""
    return render(
        request,
        "offline.html",
        {
            "page_title": "Le Mot Diplomatique est hors ligne",
            "page_description": "Page non accessible.",
        },
    )


@csrf_exempt
@require_POST
def deploy_webhook(request: HttpRequest) -> HttpResponse:
    """Un crochet pour déployer l'application après une release."""
    validate_gitlab_token(request)
    restart_service()

    return HttpResponse("Roger ça.", content_type="text/plain")


@csrf_exempt
@require_POST
def update_webhook(request: HttpRequest) -> HttpResponse:
    """Un crochet pour charger les données du mois via GitLab.

    On reçoit un job_id en paramètre et à partir de là on peut appeler l'api de GitLab
    pour récupérer les artefacts liés au job ensuite on les supprime.
    """
    validate_gitlab_token(request)
    try:
        year, month = validate_year_month(request)
        job_id = validate_job_id(request)
    except ValidationError as err:
        raise BadRequest from err

    download_update(year, month)
    load_update(year, month)
    delete_update(job_id)

    return HttpResponse("Roger ça.", content_type="text/plain")

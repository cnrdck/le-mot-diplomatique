"""Paramètres gunicorn.

@see https://docs.gunicorn.org/en/stable/settings.html#settings
"""

import multiprocessing

workers = multiprocessing.cpu_count() * 2 + 1
accesslog = errorlog = "/var/log/gunicorn/motdiplo.log"
capture_output = True

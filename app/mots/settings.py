"""Paramètres Django.

cf. https://docs.djangoproject.com/en/4.0/topics/settings/
"""

from datetime import datetime
from pathlib import Path

from django.core.management.utils import get_random_secret_key
from environs import Env

# Numéro de version
CALVER_FORMAT = "%y.%-m.%-d.%-H%M"


def version(base_dir: Path, stamp: datetime) -> str:
    """Numéro de version depuis la date de dernière modif du dossier git.

    Avec repli sur la date passée en paramètre si le dossier git manque.
    """
    path = base_dir / ".git"

    if path.exists():
        stamp = datetime.fromtimestamp(path.stat().st_mtime)
    return stamp.strftime(CALVER_FORMAT)


# Chemins
APP_DIR = Path(__file__).resolve().parent.parent
BASE_DIR = APP_DIR.parent
ARCHIVES_DIR = BASE_DIR / "archives"

# Variables d'env
# https://github.com/sloria/environs
env = Env()
env.read_env()

DEBUG = env.bool("DEBUG", False)
ENVIRONMENT = env.str("ENVIRONMENT", "production")
VERSION = version(BASE_DIR, datetime.now())


# Application & routage
INSTALLED_APPS = [
    "django.contrib.admin",
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.messages",
    "django.contrib.staticfiles",
    "django.contrib.humanize",
    "core",
    "scraper",
    "analyzer",
    "django_htmx",
]
MIDDLEWARE = [
    "django.middleware.security.SecurityMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
    "django_htmx.middleware.HtmxMiddleware",
    "csp.middleware.CSPMiddleware",
]
if ENVIRONMENT == "development":
    INSTALLED_APPS.append("silk")
    MIDDLEWARE.append("silk.middleware.SilkyMiddleware")
WSGI_APPLICATION = "mots.wsgi.application"
ROOT_URLCONF = "mots.urls"


# Gabarits & fichiers statiques
TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [],
        "APP_DIRS": True,
        "OPTIONS": {
            "context_processors": [
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "django.contrib.auth.context_processors.auth",
                "django.contrib.messages.context_processors.messages",
                "csp.context_processors.nonce",
            ],
        },
    },
]
STATIC_URL = "static/"
STATIC_ROOT = BASE_DIR / "static"
TAILWIND_INPUT_CSS = "app/core/templates/tailwind.css"
TAILWIND_OUTPUT_CSS = "app/core/static/css/style.css"


# BDD & Cache
DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.sqlite3",
        "NAME": BASE_DIR / "db.sqlite3",
    }
}
DEFAULT_AUTO_FIELD = "django.db.models.BigAutoField"
CACHES = {
    "default": {
        "BACKEND": "diskcache.DjangoCache",
        "LOCATION": BASE_DIR / "cache",
        "TIMEOUT": 30 * 24 * 60 * 60,  # 30j
    },
}


# Locales
LANGUAGE_CODE = "fr-fr"
TIME_ZONE = "Europe/Paris"
USE_I18N = True
USE_TZ = True


# Sécurité, email et reporting d'erreur
SECRET_KEY = env("SECRET_KEY", get_random_secret_key())
ALLOWED_HOSTS = env.list("ALLOWED_HOSTS", ["mot-diplomatique.localhost"])

if ENVIRONMENT == "production":
    SESSION_COOKIE_SECURE = True
    CSRF_COOKIE_SECURE = True
    SECURE_SSL_REDIRECT = True
    SECURE_HSTS_SECONDS = 3600
    SECURE_HSTS_INCLUDE_SUBDOMAINS = True
    SECURE_HSTS_PRELOAD = True
    DEBUG = False

CSP_DEFAULT_SRC = ["'none'"]
CSP_STYLE_SRC = [
    "'self'",
    "'unsafe-inline'",  # css inséré par htmx (indicateurs...)
]
CSP_SCRIPT_SRC = ["'self'", "'unsafe-eval'", "'unsafe-inline'"]
CSP_IMG_SRC = ["'self'"]
CSP_CONNECT_SRC = ["'self'"]  # appels htmx
CSP_MANIFEST_SRC = ["'self'"]
CSP_WORKER_SRC = ["'self'"]
CSP_INCLUDE_NONCE_IN = ["script-src"]


ADMIN_PATH = env.str("ADMIN_PATH", "gestion/")
ADMIN_USERNAME = env.str("ADMIN_USERNAME", "admin")
ADMIN_PASSWORD = env.str("ADMIN_PASSWORD")
ADMIN_EMAIL = env("ADMIN_EMAIL", "admin@mot-diplomatique.localhost")

ADMINS = [("Administration Mot Diplomatique", ADMIN_EMAIL)]
SERVER_EMAIL = ADMIN_EMAIL
DEFAULT_FROM_EMAIL = SERVER_EMAIL
EMAIL_BACKEND = env.str(
    "EMAIL_BACKEND", "django.core.mail.backends.console.EmailBackend"
)
EMAIL_HOST = env.str("EMAIL_HOST", "localhost")
EMAIL_HOST_USER = env.str("EMAIL_HOST_USER", "")
EMAIL_HOST_PASSWORD = env.str("EMAIL_HOST_PASSWORD", "")
EMAIL_PORT = env.int("EMAIL_PORT", 25)
if EMAIL_PORT == 465:
    EMAIL_USE_SSL = True


# Accès au site du Monde Diplomatique
LMD_EMAIL = env("LMD_EMAIL")
LMD_PASSWORD = env("LMD_PASSWORD")
LMD_CSRF = env("LMD_CSRF")


# Déploiement
GITLAB_TOKEN = env.str("GITLAB_TOKEN")
WEBHOOK_DEPLOY_PATH = env.str("WEBHOOK_DEPLOY_PATH")
WEBHOOK_UPDATE_PATH = env.str("WEBHOOK_UPDATE_PATH")
CANONICAL_URL = f"https://{ALLOWED_HOSTS[0]}"
GITLAB_API_BASE_URL = "https://gitlab.com/api/v4"
GITLAB_PROJECT_ID = env.str("CI_PROJECT_ID")
GITLAB_API_URL = f"{GITLAB_API_BASE_URL}/projects/{GITLAB_PROJECT_ID}"

# spaCy
# https://spacy.io/models/fr
NLP_MODEL = "fr_core_news_lg"

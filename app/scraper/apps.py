"""Scraper."""

from django.apps import AppConfig


class ScraperConfig(AppConfig):
    """Config."""

    default_auto_field = "django.db.models.BigAutoField"
    name = "scraper"

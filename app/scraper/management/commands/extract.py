"""Extraction du contenu des archives."""

from csv import DictWriter

from core.management.commands import YearMonthCommand
from django.conf import settings
from tqdm import tqdm

from scraper.helpers import (
    get_content,
    get_id,
    get_published_on,
    get_soup,
    get_title,
    get_url,
    handle_special_cases,
)


class Command(YearMonthCommand):
    """Extraction du contenu des archives."""

    def work_on(self, year: str, month: str) -> None:
        """Parcours les archives locales par date et extrait le contenu d'un article."""
        path = settings.ARCHIVES_DIR / year / month

        articles_csv = path / "articles.csv"
        if articles_csv.exists() and not self.forced:
            tqdm.write(f"{month}/{year} déjà fait")
            return

        files = sorted(path.glob("*.html"))
        progress = tqdm(files)
        articles: list[dict[str, str | object]] = []

        for article_file in progress:
            progress.set_description(f"{month}/{year}")
            soup = get_soup(article_file.read_text())
            url = get_url(soup)
            article_id = get_id(url)
            article = {
                "id": article_id,
                "published_on": get_published_on(soup),
                "url": url,
                "title": get_title(soup),
            }
            content_file = path / f"{article_id}.txt"
            with content_file.open("w") as f:
                content = get_content(soup)
                content = handle_special_cases(article_id, content)
                f.write(content)
            articles.append(article)
        if articles:
            with articles_csv.open("w", newline="") as f:
                writer = DictWriter(f, ["id", "published_on", "url", "title"])
                writer.writeheader()
                writer.writerows(articles)

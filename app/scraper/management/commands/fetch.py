"""Récupération des articles depuis le site du diplo."""

from core.management.commands import YearMonthCommand
from django.conf import settings
from tqdm import tqdm

from scraper.helpers import (
    find_articles_links,
    get_id,
    get_login_cookies,
    login,
    save_article,
    save_login_cookies,
)


class Command(YearMonthCommand):
    """Crawler du diplo."""

    def pre_loop(self) -> None:
        """Récupération des cookies."""
        if cookies := get_login_cookies(settings.BASE_DIR):
            tqdm.write("Cookies d'authentification récupérés.")
        else:
            cookies = login(
                settings.LMD_EMAIL, settings.LMD_PASSWORD, settings.LMD_CSRF
            )
            tqdm.write("Cookies d'authentification régénérés.")
            save_login_cookies(cookies, settings.BASE_DIR)
        self.cookies = cookies

    def work_on(self, year: str, month: str) -> None:
        """Parcours une archive du diplo par date et télécharge les articles."""
        path = settings.ARCHIVES_DIR / year / month
        path.mkdir(parents=True, exist_ok=True)
        links = find_articles_links(year, month)
        if not links:
            tqdm.write(f"Aucun article pour {month}/{year}")
            return
        progress = tqdm(links)
        progress.set_description(f"{month}/{year}")
        for url in progress:
            article_id = get_id(url)
            article_html = path / f"{article_id}.html"
            if article_html.exists() and not self.forced:
                tqdm.write(f"{url} déjà fait")
                return
            save_article(url, self.cookies, path)

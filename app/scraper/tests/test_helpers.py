import json
from datetime import datetime
from unittest.mock import Mock, patch

import pytest
from bs4 import BeautifulSoup
from requests.cookies import RequestsCookieJar

from scraper.helpers import (
    BASE_URL,
    LOGIN_COOKIES,
    cleanup_commas,
    cleanup_dashed_initialism,
    cleanup_dashed_quotes,
    cleanup_dashes,
    cleanup_dotted_acronyms,
    cleanup_line_and_spaces,
    cleanup_m_dot,
    cleanup_parenthesis,
    cleanup_quotes_and_parenthesis,
    cleanup_slashes,
    cleanup_urls,
    find_articles_links,
    get_content,
    get_id,
    get_login_cookies,
    get_paragraphs,
    get_published_on,
    get_soup,
    get_title,
    get_url,
    handle_special_cases,
    normalize_utf8,
    replace_punctuation,
    sanitize,
    save_article,
    save_login_cookies,
)


@pytest.fixture
def html():
    return """<html>
      <head>
        <title>Titre, par Jeanmich (Le Monde diplomatique, mois 1999)</title>
        <meta property="og:url" content="https://mot.fr/2022/04/ABC/123" />
        <meta property="article:published_time" content="2022-04-01" />
      </head>
      <body>
        <header>Ignoré</header>
        <div class="contenu-principal">
          <div class="chapo">
            <p>chapo</p>
          </div>
          <div class="texte">
            <p>Premier paragraphe</p>
            <p>Second paragraphe</p>
          </div>
        </div>
        <footer>Ignoré</footer>
      </body>
    </html>"""


@pytest.fixture
def soup(html):
    return get_soup(html)


@pytest.fixture
def url(soup):
    return get_url(soup)


def test_get_soup(html):
    assert isinstance(get_soup(html), BeautifulSoup)


def test_get_url(soup):
    assert get_url(soup) == "https://mot.fr/2022/04/ABC/123"


def test_get_id(url):
    assert get_id(url) == 123


def test_get_title(soup):
    assert get_title(soup) == "Titre, par Jeanmich"


def test_get_published_on(soup):
    assert get_published_on(soup) == datetime(2022, 4, 1).date()


@pytest.mark.parametrize(
    ("article_id", "content", "expected"),
    [
        (59366, "testRetrouvez ci-dessous", "test"),
        (1, "testRetrouvez ci-dessous", "testRetrouvez ci-dessous"),
    ],
)
def test_handle_special_cases(article_id, content, expected):
    content = "test" + "Retrouvez ci-dessous"
    assert handle_special_cases(article_id, content) == expected


def test_normalize_utf8():
    text = "test\xa0test"
    assert normalize_utf8(text) == "test test"


def test_cleanup_line_and_spaces():
    text = """a
    b  c d
    e
    f              g"""
    assert cleanup_line_and_spaces(text) == "a b c d e f g"


def test_cleanup_dashes():
    text = "ban -ane"
    assert cleanup_dashes(text) == "ban-ane"


def test_cleanup_m_dot():
    text = "M.Banane"
    assert cleanup_m_dot(text) == "M. Banane"


def test_replace_punctuation():
    text = "banane..."
    assert replace_punctuation(text) == "banane…"


def test_get_content(soup):
    assert get_content(soup) == "chapo\n\nPremier paragraphe\n\nSecond paragraphe"


class TestGetParagraphs:
    def test_normal(self):
        html = "<p>a</p><p>b</p>"
        soup = get_soup(html)
        assert get_paragraphs(soup) == ["a", "b"]

    def test_empty(self):
        html = ""
        soup = get_soup(html)
        assert not get_paragraphs(soup)

    def test_falsy(self):
        assert not get_paragraphs("")


def test_save_login_cookies(tmp_path):
    class FakeJar(RequestsCookieJar):
        def get_dict(self):  # type: ignore
            return {"test": "ok"}

    jar = FakeJar()
    save_login_cookies(jar, tmp_path)
    json_cookie = tmp_path / LOGIN_COOKIES
    assert json.loads(json_cookie.read_text()) == {"test": "ok"}


class TestSanitize:
    def test_basic(self):
        assert sanitize("test") == "test"

    @pytest.mark.parametrize(
        "method",
        [
            "normalize_utf8",
            "cleanup_line_and_spaces",
            "cleanup_dashes",
            "cleanup_quotes_and_parenthesis",
            "cleanup_m_dot",
            "cleanup_dashed_initialism",
            "cleanup_dashed_quotes",
            "cleanup_dotted_acronyms",
            "cleanup_urls",
            "cleanup_slashes",
            "cleanup_commas",
            "cleanup_parenthesis",
            "replace_punctuation",
        ],
    )
    def test_calls(self, method):
        with patch(f"scraper.helpers.{method}", return_value="text") as mock:
            sanitize("text")
            mock.assert_called_once_with("text")


@pytest.mark.parametrize(
    "sign",
    ['"', "«", "»", "("],
)
def test_cleanup_quotes_and_parenthesis(sign):
    assert (
        cleanup_quotes_and_parenthesis(f"before{sign}after") == f"before {sign} after"
    )


@pytest.mark.parametrize(
    ("text", "expected"),
    [
        ("U.R.S.S.-États-Unis", "U.R.S.S. - États-Unis"),
        ("a.-bristol", "a. - bristol"),
    ],
)
def test_cleanup_dashed_initialism(text, expected):
    assert cleanup_dashed_initialism(text) == expected


def test_cleanup_dashed_quotes():
    assert cleanup_dashed_quotes('anti-"Cosby Show"') == 'anti - " Cosby Show"'


def test_cleanup_dotted_acronyms():
    assert cleanup_dotted_acronyms("test C.N.R.S.") == "test CNRS"


def test_cleanup_urls():
    assert cleanup_urls("européennehttp://ue.eu.int") == "européenne http://ue.eu.int"


def test_cleanup_slashes():
    assert cleanup_slashes("aîné/cadet") == "aîné / cadet"


def test_cleanup_commas():
    assert cleanup_commas("partisan,ce site") == "partisan, ce site"


def test_cleanup_parenthesis():
    assert cleanup_parenthesis("adolescent(e") == "adolescent ( e"


class TestFindArticlesLinks:
    def test_ok(self):
        with patch("scraper.helpers.requests") as mock_requests:
            mock_response = Mock()
            mock_response.text = """<div class="contenu-principal">
                <a href="/2000/01/test1">test1</a>
                <a href="/bad/path">bad path</a>
                <a href="#ignore">ignore</a>
                <a href="/2000/01/test2">test2</a>
                <p>bonjour</p>
            </div>"""
            mock_requests.get = Mock(return_value=mock_response)
            links = find_articles_links("2000", "01")
            mock_requests.get.assert_called_once_with(f"{BASE_URL}/2000/01/", timeout=5)
            assert len(links) == 2
            assert links[0] == f"{BASE_URL}/2000/01/test1"
            assert links[1] == f"{BASE_URL}/2000/01/test2"

    def test_no_article(self):
        with patch("scraper.helpers.requests") as mock_requests:
            mock_response = Mock()
            mock_response.text = """<div class="not-contenu-principal">
                <a href="/2000/01/test1">test1</a>
                <a href="/2000/01/test2">test2</a>
            </div>"""
            mock_requests.get = Mock(return_value=mock_response)
            assert not find_articles_links("2000", "01")


def test_save_article(tmp_path):
    with patch("scraper.helpers.requests") as mock_requests:
        mock_response = Mock()
        mock_response.content = b"coucou"
        mock_requests.get = Mock(return_value=mock_response)
        save_article(
            "https://www.monde-diplomatique.fr/2022/01/RAMIREZ/64240",
            RequestsCookieJar(),
            tmp_path,
        )
    result = tmp_path / "64240.html"
    assert result.read_bytes() == b"coucou"


class TestGetLoginCookies:
    def test_cookie_file_does_not_exists(self, tmp_path):
        assert get_login_cookies(tmp_path) is None

    def test_invalid_cookie_file(self, tmp_path):
        cookie_file = tmp_path / LOGIN_COOKIES
        cookie_file.write_text("pas du json")
        with patch("scraper.helpers.json", side_effect=Exception()) as mock_json:
            mock_json.load.side_effect = Exception()
            assert get_login_cookies(tmp_path) is None
            mock_json.load.assert_called_once()

        assert not cookie_file.exists()

    def test_expired_cookie(self, tmp_path):
        cookie_file = tmp_path / LOGIN_COOKIES
        cookie_file.write_text('{"json": "oui"}')
        with patch("scraper.helpers.cookiejar_from_dict") as mock_cookiejar_from_dict:
            mock_jar = Mock()
            mock_jar.get.return_value = None
            mock_cookiejar_from_dict.return_value = mock_jar
            assert get_login_cookies(tmp_path) is None
            mock_jar.clear_expired_cookies.assert_called_once()
            mock_jar.get.assert_called_once_with("PHPSESSID")
        assert not cookie_file.exists()

    def test_ok(self, tmp_path):
        cookie_file = tmp_path / LOGIN_COOKIES
        cookie_file.write_text('{"json": "oui"}')
        with patch("scraper.helpers.cookiejar_from_dict") as mock_cookiejar_from_dict:
            mock_jar = Mock()
            mock_jar.get.return_value = True
            mock_cookiejar_from_dict.return_value = mock_jar
            assert get_login_cookies(tmp_path) == mock_jar
        assert cookie_file.exists()

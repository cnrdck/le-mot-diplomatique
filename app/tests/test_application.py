import pytest
from django.urls import Resolver404
from mots.asgi import application as async_application
from mots.wsgi import application


def test_handles_request(rf):
    with pytest.raises(Resolver404):
        application.resolve_request(rf.get("/test_handles_request"))


@pytest.mark.asyncio
async def test_async_handles_request(async_rf):
    with pytest.raises(Resolver404):
        await async_application.resolve_request(async_rf.get("/test_handles_request"))
